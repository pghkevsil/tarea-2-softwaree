import ddf.minim.*;
Minim minim;
AudioPlayer player;

float aceleracion=0; //Variable aceleracion inicializada en 0
float frenado=0;     //Variable frenado inicializada en 0
float velocidad=0;   //Variable velocidad inicializada en 0
float tiempo=0;      //Variable tiempo inicializada en 0

void setup(){
size(1200,800);                            //Tamaño de la ventana
minim = new Minim(this);                   //Función minim
player = minim.loadFile("alarm.mp3");}     //Sonido escogido

void draw(){
background(100,200,100);  //Color del fondo
fill(15);                 //Color para texto
textSize(60);             //Tamaño de fuente para el sensor de frenado
text("Frenado",200,120);
textSize(60);             //Tamaño fuente para el sensor de aceleración
text("Aceleración",800,120);
textSize(60);             //Tamaño fuente para el sensor de aceleración
text("Velocidad",480,120);

fill(200);                
text(frenado,270,170);     //Visualizador del valor de frenado
fill(200);
text(aceleracion,860,170); //Visualizador del valor de la aceleración
fill(200);
text(velocidad,560,170);   //Visualizador del valor de la velocidad

float distancia3=dist(mouseX,mouseY,900,300); //Variable entera para captar la posición del mouse, puntero en el acelerador
if (distancia3<100){
velocidad=velocidad+0.5;                          //Aumento de aceleracionatura gradual
fill(200,0,200);                                //Color del sensor cuando el cursor esta por encima
}
else {velocidad=velocidad;} //Condición si el mouse se encuentra fuera del área
if (frenado>0){velocidad=velocidad-0.5;
fill(velocidad);            //Visualizador gradual velocidad
fill(200,0,0);
ellipse(600,300,200,200);}  //Luz roja que indica el frenado
if (velocidad<0){velocidad=0;} 

float distancia2=dist(mouseX,mouseY,900,300);//Variable entera para captar la posición del mouse, puntero en el acelerador
fill(0); stroke(100,0,0); strokeWeight(0);    
if (distancia2<100){;
aceleracion=aceleracion+0.5;                     //Aumento de aceleración gradual
fill(200,0,200);                               //Color del sensor cuando el cursor esta sobre el punto de contacto
if (distancia2 < 100){tiempo=tiempo+1;}
if (tiempo==555){player.play(10);}
}
else {aceleracion=aceleracion-0.5;tiempo=0;}     //Condición si el mouse se encuentra fuera del área
if (aceleracion<0){aceleracion=0;}   
if (aceleracion>277.5){aceleracion=277.5;}  
ellipse(900, 300, 200, 200);                   //Sensor de aceleración

float distancia = dist(mouseX,mouseY,300,300);// Variable entera para captar la posicion del mouse en los sensores
fill(0); stroke(100,100,0); strokeWeight(0);
if (distancia<100){
  frenado=frenado+0.5;          //Aumento del frenado gradualmente
fill(200,0,100);}             //Color del sensor cuando el cursor está sobre el punto de contacto
else{frenado=frenado-0.5;}      //Condición si el mouse se encuentra fuera del área
if (frenado<0){frenado=0;}        
if (frenado>277.5){frenado=277.5;}   
if (velocidad==0){frenado=0;}
ellipse(300,300,200,200);     //Sensor de frenado


fill(0,frenado,0);            //Visualizador gradual del sensor de frenado
ellipse(300,550,150,150); 

fill(0,aceleracion,0);        //Visualizador gradual sensor de aceleración
ellipse(900,550,150,150); 

fill(0,velocidad,0);        //Visualizador gradual sensor de aceleración
ellipse(600,550,150,150); 

if (frenado>0){               //Texto de alerta
  textSize(50);
  fill(255);
text("¡¡FRENANDO!!",450,450);
}

if (aceleracion==277.5){
  fill(255);
  textSize(50);
text("¡¡ACELERACIÓN MÁXIMA ALCANZADA!!",220,200);     //Texto de alerta
text("¡¡ACELERACIÓN MÁXIMA ALCANZADA!!",220,400);
text("¡¡ACELERACIÓN MÁXIMA ALCANZADA!!",220,600);
}
}
